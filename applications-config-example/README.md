# Nephio "Like" to deploy and manage GCP clusters

**using KPT + Nephio UI + FluxCD + GCP + Gitlab + Crossplane**

This part aims to setup the nephio+fluxcd prepared environment to
deploy workload clusters using Crossplane and Gitlab repositories

! This is only for demo

[[_TOC_]]

## Steps

1. Create an empty cluster
2. Install FluxCD and plug it to this repository
3. Setup the environment
4. Check the installation
5. Deploy crossplane example compositions
6. Deploy your first GKE Cluster
7. Deploy an application on this new cluster

## Step 1: Create an empty cluster

Create a first cluster that will host this Nephio 'like' management cluster.
Do it where you like, there is only 2 requirements:

    - it must have enough ressources
    - it must access this repository on GitLab.

## Step 2: Install FluxCD

FluxCD is one of the references for GitOps workflow. We will now install it
and setup the initial synchronisation.

To simplify the procedure, install the FluxCD CLI on your PC:

```sh
curl -s https://fluxcd.io/install.sh | sudo bash
```

Install FluxCD controllers on the clusters

```sh
# install fluxcd
flux install --export | kubectl apply -f
```

Add the first Flux Repo+Kustomization to install nephio basic packages

```sh
# create the first source repo
flux create source git cluster-init \
    --url=https://gitlab.com/Orange-OpenSource/lfn/nephio/nephio-management/cluster-init \
    --branch=main
# set the first kustomization cluster-init \
    --source=GitRepository/cluster-init \
    --path="./" \
    --target-namespace="flux-system"
```

At the end of the installation you should have:
    - a minimal nephio system:
      - nephio-controllers
      - nephio-blueprints (set of blueprints repository)
      - nephio-gitea (not used, but required by the controller)
      - nephio-network-config
      - nephio-resource-backend
      - nephio-UI
      - porch-dev
    - crossplane (without providers)

To ensure fluxCD has finished the job:

```sh
flux get all
```

<details>
<summary>Click this to get output example</summary>

```sh
NAME                                 	REVISION          	SUSPENDED	READY	MESSAGE                                           
gitrepository/cluster-init           	main@sha1:955af908	False    	True 	stored artifact for revision 'main@sha1:955af908'
gitrepository/nephio-example-packages	main@sha1:a9119f0f	False    	True 	stored artifact for revision 'main@sha1:a9119f0f'
gitrepository/nephio-packages        	main@sha1:67cb0a7b	False    	True 	stored artifact for revision 'main@sha1:67cb0a7b'
gitrepository/nephio-test-packages   	main@sha1:0ed6a657	False    	True 	stored artifact for revision 'main@sha1:0ed6a657'

NAME                     	REVISION       	SUSPENDED	READY	MESSAGE                                     
helmrepository/crossplane	sha256:85c414ec	False    	True 	stored artifact: revision 'sha256:85c414ec'

NAME                            	REVISION	SUSPENDED	READY	MESSAGE                                         
helmchart/flux-system-crossplane	1.13.2  	False    	True 	pulled 'crossplane' chart with version '1.13.2'

NAME                  	REVISION	SUSPENDED	READY	MESSAGE                          
helmrelease/crossplane	1.13.2  	False    	True 	Release reconciliation succeeded	

NAME                                 	REVISION          	SUSPENDED	READY	MESSAGE                              
kustomization/cluster-init           	main@sha1:955af908	False    	True 	Applied revision: main@sha1:955af908
kustomization/crossplane             	main@sha1:0ed6a657	False    	True 	Applied revision: main@sha1:0ed6a657
kustomization/crossplane-providers   	main@sha1:0ed6a657	False    	True 	Applied revision: main@sha1:0ed6a657
kustomization/flux-system            	main@sha1:0ed6a657	False    	True 	Applied revision: main@sha1:0ed6a657
kustomization/nephio-blueprints      	main@sha1:955af908	False    	True 	Applied revision: main@sha1:955af908
kustomization/nephio-controllers     	main@sha1:a9119f0f	False    	True 	Applied revision: main@sha1:a9119f0f
kustomization/nephio-gitea           	main@sha1:a9119f0f	False    	True 	Applied revision: main@sha1:a9119f0f
kustomization/nephio-network-config  	main@sha1:a9119f0f	False    	True 	Applied revision: main@sha1:a9119f0f
kustomization/nephio-resource-backend	main@sha1:a9119f0f	False    	True 	Applied revision: main@sha1:a9119f0f
kustomization/nephio-webui           	main@sha1:67cb0a7b	False    	True 	Applied revision: main@sha1:67cb0a7b
kustomization/porch-dev              	main@sha1:a9119f0f	False    	True 	Applied revision: main@sha1:a9119f0f
```

</details>

You can now connect to Nephio UI:

```sh
kubectl port-forward --namespace=nephio-webui svc/nephio-webui 7007
```

Open <http://localhost:7007> and visit the place.


## Step 3: Setup the environment

This step will prepare all the secrets and finalize the configuration.

It requires:

| variable         | description                                                                              | example                                                      |
|------------------|------------------------------------------------------------------------------------------|--------------------------------------------------------------|
| GCP_PROJECT      | The name of your target GCP project                                                      | `ino-it-s-noa-onap-cd-sbx`                                   |
| GCP_SA           | The GCP Service Account to create that will be plugged to Crossplane to manage the infra | `nephio-crossplane-infra`                                    |
| GITLAB_HOST      | The url of the Gitlab server                                                             | `gitlab.com`                                                 |
| GITLAB_USER      | The name of the Gitlab user                                                              | `john.doe`                                                   |
| GITLAB_PASSWORD  | The password of the Gitlab user                                                          | `john.doe`                                                   |
| GITLAB_GROUPID   | The id of the Gitlab group that will host your deployment repositories                   | `123456789`                                                  |
| GITLAB_GROUPPATH | The path of the same gitlab group                                                        | `gitlab.com/Orange-OpenSource/lfn/nephio/nephio-deployments` |

Use the `management.tpl.env` to create a filled `management.tpl` file.

Then run
```sh
mv management.tpl.env management.tpl
vim management.tpl
bash run_me.sh
```

To check the configuration:

- go to the nephio-ui and ensure you a `management` repository is available
- go to your gitlab group ($GITLAB_GROUPPATH), and ensure a gitlab project
  with name `management` is available.

## Step 5: Deploy GKE crossplane example compositions

To create a GKE cluster and underlying networking part, we will use a Crossplane
composition. This could have been done using the `run_me.sh` script, but there
is no fun. Lets discover how Nephio is working.

To deploy our GKE Crossplane Composition, in a Nephio and GitOps way, we have
to declare it in a Git. This Git repo was created in the previous step, we just
have to fill it.

In a Nephio way, we will use Kpt/Porch mechanisms to copy and transform a
prepared blueprint to the target repo.

The blueprint we will use is named [`deploy-crossplane-gke-compositions`](
https://gitlab.com/Orange-OpenSource/lfn/nephio/packages/nephio-test-packages/-/tree/main/deploy-crossplane-gke-compositions).
It contains a FluxCD kustomization the will deploy
[`crossplane-gke-compositions`](https://gitlab.com/Orange-OpenSource/lfn/nephio/packages/nephio-test-packages/-/tree/main/crossplane-gke-compositions)
(the composition themselves)

**Why using FluxCD Kustomization and not copy directly the compositions**(
as a traditional Nepĥio Way): because I prefer to manage pointers to templates
than a copy of ressources themselves. It is easier to manage upgrades (no
more Kpt/Porch merging issues) and allow to upgrade a whole set of deployments
in a easier way. This is an implementation choice, for your need, do as you
want.

To deploy the blueprint:

### Using the UI

- In the `Deployments` box, select `management`
- At the top left `Add deployment`
- Action options:

  - `Create a new deployment by cloning a team blueprint`
  - `nephio-test-packages`
  - `deploy-crossplane-gke-compositions`

- Keep all other option like this, then select `create deployment`

### Using a Porch Package Variant

Create and apply this Package Variant to ask Porch to clone
`deploy-crossplane-gke-compositions` blueprint to `management` repo

```sh
cat << EOF > pv-deploy-crossplane-gke-compositions.yaml
---
apiVersion: config.porch.kpt.dev/v1alpha1
kind: PackageVariant
metadata:
  name: deploy-crossplane-gke-compositions
spec:
  annotations:
    approval.nephio.org/policy: initial
  upstream:
    package: deploy-crossplane-gke-compositions
    repo: nephio-test-packages
    revision: v1.1.2
  downstream:
    package: deploy-crossplane-gke-compositions
    repo: management
  pipeline:
    mutators:
    - image: gcr.io/kpt-fn/set-annotations:v0.1.4
      configMap:
        nephio.org/cluster-name: management
EOF
kubectl apply -f pv-deploy-crossplane-gke-compositions.yaml
```

or do it using kpt cli:

```sh
export pkgId=$(
    kpt alpha rpkg get --name deploy-crossplane-gke-compositions -o json |
    jq -r 'select(.spec.lifecycle == "Published" and
                  .metadata.labels["kpt.dev/latest-revision"] == "true" and
                  .spec.repository == "nephio-test-packages").metadata.name'
    )
kpt alpha rpkg clone -n default --repository management \
    $pkgId deploy-crossplane-gke-compositions
```

<details>
<summary>
Nephio-controller shall merge the package to main, if not follow this procedure
</summary>

go to interface and `propose` then `approve` the deployment. Or use the cli:

```sh
export deplPkgId=$(
    kubectl get -n default packagerevs -o yaml |
    yq -r '.items[] | select(
        .metadata.labels["internal.porch.kpt.dev/repository"] == "management"
    and (
        .metadata.ownerReferences[0].uid |
        contains("deploy-crossplane-gke-compositions")
        )
    ).metadata.name'
)
kpt alpha rpkg propose -n default $deplPkgId
kpt alpha rpkg approve -n default $deplPkgId
```

</details>

### Check deployment

This process has:

- push a blueprint to a git deployment project
- deployed a new FluxCD Kustomize object
- deployed crossplane compositions

The `management` repo now contains a new folder
[`deploy-crossplane-gke-compositions`](
https://gitlab.com/Orange-OpenSource/lfn/nephio/nephio-deployments/management/-/tree/main/deploy-crossplane-gke-compositions
)

Check fluxcd deployement:

```sh
❯❯❯ kubectl get -n flux-system kustomization deploy-crossplane-gke-compositions
NAME                                 AGE     READY   STATUS
deploy-crossplane-gke-compositions   6m23s   True    Applied revision: main@sha1:0ed6a657f0690e5a0eac87d11a3b3e6e8b91aa79
```

Check crossplane compositions
```sh
❯❯❯ kubectl get compositions
NAME                                                       XR-KIND            XR-APIVERSION                                 AGE
gcp.compositenetworks.multik8s.platformref.crossplane.io   CompositeNetwork   multik8s.platformref.crossplane.io/v1alpha1   7m3s
gke.multik8s.platformref.crossplane.io                     GKE                multik8s.platformref.crossplane.io/v1alpha1   7m3s
```

## Step 6: Deploy your first GKE Cluster

In a same way, we will now deploy a GKE Cluster !

How ? Just deploying using Kpt/Porch a new Flux kustomization that will:

- Deploy a GCP VPC and create a GKE cluster on it
- Create a Gitlab project
- Add a local kpt/repositoy to manage this new cluster

Do it using the Nephio UI or

```sh
cat << EOF > pv-deploy-workload-gke-cluster-workload001.yaml
---
apiVersion: config.porch.kpt.dev/v1alpha1
kind: PackageVariant
metadata:
  name: deploy-workload-gke-cluster-workload001
spec:
  annotations:
    approval.nephio.org/policy: initial
  upstream:
    package: deploy-workload-gke-cluster
    repo: nephio-test-packages
    revision: v1.1.3
  downstream:
    package: cluster-workload001
    repo: management
  packageContext:
    data:
      region: europe-west9
      nodesCount: "3"
      nodesSize: small
      visibility: public
EOF
kubectl apply -f pv-deploy-workload-gke-cluster-workload001.yaml
```

<details>
<summary>Click this to understand why we have a `package-variables.yml` file
</summary>

Today, when deploying a package using the UI, the `package-context.yaml` file
is overwritten erasing all variables used by the setter function. But the
`package-variables.yml` is not editable using a `Package Variant`. That is the
reason why we have 2 setter functions, one for the UI, the other for the
`Package variant` 🤔🤔🤔

</details>

You can check:

- A new cluster is created on your GCP account
- A new git repo is created like:
  `gitlab.com/Orange-OpenSource/lfn/nephio/nephio-deployments/workload001`
- this repo is managed by nephio with name `workload001`

## Step 7: Deploy an application on this new cluster

Shall I explain you how to do this ? Just use the same procedure to deploy
`nginx/v1` from `nephio-basic-packages` on this new `workload001` repository.

## Resume diagram

This is how the full deployment looks like:

![example diagram](deploy-workload-gke-cluster.drawio.png)

## Repository cleanup

To cleanup a whole repo, here is the fish lines to apply

```sh
set repo management
for name in (kg packagerevs -o yaml | yq -r '.items[] | select(.metadata.labels["internal.porch.kpt.dev/repository"] == "'$repo'").metadata.name');
    kpt alpha rpkg propose-delete -n default $name;
    kpt alpha rpkg delete $name -n default
end
```
