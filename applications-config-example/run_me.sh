#!/bin/env bash

source management.env

# Those commands are using 1Password cli to access secret. Replace all `op`
# command with your own secrets

kubectl create secret generic gitlab-credentials -n crossplane \
    --from-literal=token=${GITLAB_PASSWORD}

kubectl create secret generic gitlab-auth \
    -n default\
    --type='kubernetes.io/basic-auth' \
    --from-literal=username=${GITLAB_USER} \
    --from-literal=password=${GITLAB_PASSWORD}

# Prepare the GCloud SA for crossplane gcp
if not gcloud iam service-accounts describe ${GCP_SVC_ACCT}; then
  gcloud iam service-accounts create --project ${GCP_PROJECT} ${GCP_SA}
  gcloud projects add-iam-policy-binding ${GCP_PROJECT} --member "serviceAccount:${GCP_SVC_ACCT}" --role="roles/iam.serviceAccountUser"
  gcloud projects add-iam-policy-binding ${GCP_PROJECT} --member "serviceAccount:${GCP_SVC_ACCT}" --role="roles/container.admin"
  gcloud projects add-iam-policy-binding ${GCP_PROJECT} --member "serviceAccount:${GCP_SVC_ACCT}" --role="roles/compute.networkAdmin"

  gcloud iam service-accounts keys create gcp_private_key.json \
      --iam-account=${GCP_SVC_ACCT}
fi

kubectl create secret \
    generic gcp-secret \
    -n crossplane \
    --from-file=creds=./gcp_private_key.json

# Set the gke-env file

echo """
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: cluster-vars
  namespace: flux-system
data:
  project: ${GCP_PROJECT}
  location: ${GCP_LOCATION}
  gitGroupId: '${GITLAB_GROUPID}'
  gitGroupPath: ${GITLAB_GROUPPATH}
""" > cluster-vars.configmap.yml

# Apply all

kubectl apply -k .